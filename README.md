# Avaliação 2
Aluno: Thiago Oppermeier
Professor: Edson Funke
Disciplina: Manutenção e configuração de software

 

Trabalho sobre configuração e instalação de um ambiente apache:

Neste projeto será instalado o servidor APACHE2, juntamente com o gerenciador de banco de dados MySQL, e um software de consulta SQL.
Versões a serem instaladas: APACHE 2.4 - MySQL 8.0.21.
Distribuição de SO: Ubuntu 20.04.

Será necessário um servidor com SO Debian instalado, e espaço de armazenamento disponível de 500GB.
• Primeiro devem verificados se todos pré-requisitos para a instalação do APACHE e do MySQL, estão
completos, em seguida poderá ser feita a instalação e posteriormente a configuração dos softwares.


# Ambiente APACHE:


O servidor Apache ou Servidor HTTP Apache é um servidor Web , usado principalmente no Linux. É bastante popular em servidores de páginas desde o inicio da Internet e segundo estatísticas é o mais usado em servidores Web atualmente, superando o Windows. O Apache é compatível com PHP, Perl, CGI e ASP, bastando para isso instalar os módulos adequados. 

Neste trabalho ensinarei como instalar e fazer a configuração básica do servidor web Apache, criar ambiente de hospedagem para múltiplos sites e criar logs de acessos e erros.Utilizaremos o sistema operacional Linux Ubuntu 20.04.

Antes de iniciar a instalação, verifique as atualizações.

# apt-get update

Para instalá-lo digite o comando:

# apt-get install apache2 

Use o comando “whereis” para descobrir onde estão os arquivos de instalação.

# whereis apache2

O retorno do comando será:

/usr/sbin/apache2 /etc/apache2 /usr/lib/apache2 /usr/share/apache2 /usr/share/man/man8/apache2.8.gz

Onde:

/usr/sbin/apache2 é o diretório do binário principal do apache

/usr/lib/apache2 é a biblioteca do apache2

/usr/share/apache são os objetos padrões do apache

/usr/share/man/man8/apache2.8.gz é o manual do apache

Para verificar os serviços do apache, utilize o comando a seguir:

#ps ax | grep apache

Após iniciar o Apache, você pode testá-lo: abra um navegador de internet e digite o endereço ip da sua interface de rede no campo de endereço: http://<IP>). Se o Apache estiver funcionando corretamente, uma página como a mostrada abaixo será exibida no browser:

2020_11_Thiago_Oppermeier_Servidor_Apache_Linux_Ubuntu20.04

Criando e habilitar o domínio (lab.apache.com.br)

adicione no arquivo /etc/hosts uma entrada:

192.168.2.220   lab.apache.com.br  #    – IP de teste.

# vi /etc/apache2/sites-available/lab.apache.com.br

NameVirtualHost lab.apache.com.br
<VirtualHost lab.apache.com.br >
DocumentRoot /var/www/lab.apache
ServerName lab.apache.com.br
ServerAdmin webmaster@lab.apache.com.br
ErrorLog lab.apache.com.br-error.log
CustomLog lab.apache.com.br-access.log common
</VirtualHost>

Habilite o site:

# a2ensite lab.apache.com.br

Para criar seu site

Crie o diretório do seu site:

# mkdir /var/www/lab

Crie um arquivo index.html dentro deste diretório contendo qualquer conteúdo HTML.

# vi index.html

<html><body><h1>Página teste</h1></body></html>

Reinicie o Apache2:

# /etc/init.d/apache2 restart

Fizemos a configuração basica de um servidor web e a criação de um virtual host.



# Aplicativo

Instalação do JAVA:

Abra seu terminal (ctrl + alt + T) e cole os comandos abaixo:

sudo add-apt-repository ppa:webupd8team/java

sudo apt-get update

sudo apt-get install oracle-java8-installer

Se quiser saber qual é a versão do Java instalado,cole o comando no terminal, 

$ java -version






O MySQL é um sistema que gerencia banco de dados. Ele utiliza a linguagem SQL (Structure Query Language – Linguagem de Consulta Estruturada), que é a linguagem mais popular para inserir, acessar e gerenciar o conteúdo de um banco de dados.


Aqui ensinarei como instalar, acessar, criar e inserir dados no MySQL Server no Linux Ubuntu 20.04.

1 – Antes de iniciar a instalação, verifique as atualizações.

# apt-get update

2 – Para instalá-lo digite o comando:

# apt-get install mysql-server 

Diga sim para a pergunta que será exibida na tela e espere até o final do download, ao iniciar a instalação é solicitada a senha de usuário root para o Mysql. Crie uma senha, confirme e aguarde o fim da instalação.

3 – Finalizada a instalação, faça um teste:

Digite o seguinte comando para acessar o Mysql, substitua “SENHA” pela senha que criou.

# mysql -u root –pSENHA

mysql> show databases;
2020_11_Thiago_Oppermeier_show_database_1


4 – Crie uma base de dados:

mysql> create database Seguradora;

5 – Depois que a base de dados foi criada, podemos acessa-la e criar tabelas, usando o comando “use” mais o nome do base criada:

mysql> use Seguradora

6 – Criar tabela:

mysql> create table usuarios( nome text, login varchar (30), senha varchar (10));

CREATE TABLE veiculo(cdplaca serial NOT null,					
 nranofabricacao integer not null,
 nranomodelo integer not null,
 dsmodelo varchar (30),
 vlfipe integer not null,
CONSTRAINT pk_veiculo PRIMARY KEY (cdplaca));

 
 CREATE TABLE pessoa(codpessoa integer NOT NULL,					
nmpessoa varchar(150) NOT NULL,
nmrcpf integer not null,
dtexpedicaocnh date NOT NULL,
dtnascimento date NOT NULL,
nrcnh integer NOT NULL, 
CONSTRAINT pk_pessoa PRIMARY KEY (codpessoa),
CONSTRAINT un_nmrcpf_pessoa UNIQUE (nmrcpf))

8 – Fazendo a consulta para verificar os dados inseridos

select a.cdplaca, a.dsmodelo
from a.veiculo

select p.nmpessoa
from p.pessoa

select a.nranofabricacao, a.nranomodelo
from a.veiculo












